// Abeliangroup defines the abelian group interface and common functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package abeliangroup

import (
	"bitbucket.org/pcasmath/abelianmonoid"
	"bitbucket.org/pcasmath/object"
)

// Interface defines the interface that all abelian groups must satisfy.
type Interface interface {
	abelianmonoid.Interface
	// Subtract returns x - y.
	Subtract(x object.Element, y object.Element) (object.Element, error)
	// Negate returns -x.
	Negate(x object.Element) (object.Element, error)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.  If G satisfies the interface:
//		type Sumer interface {
//			// Sum returns the sum of the elements in the slice S. The
//			// sum of the empty slice is the zero element.
//			Sum(S ...object.Element) (object.Element, error)
//		}
// then G's Sum method will be called.
func Sum(G Interface, S ...object.Element) (object.Element, error) {
	return abelianmonoid.Sum(G, S...)
}
