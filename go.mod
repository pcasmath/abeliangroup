module bitbucket.org/pcasmath/abeliangroup

go 1.16

require (
	bitbucket.org/pcasmath/abelianmonoid v0.0.1
	bitbucket.org/pcasmath/object v0.0.4
)
